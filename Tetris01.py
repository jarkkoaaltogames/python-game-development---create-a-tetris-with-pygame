import pygame, sys

from pygame.locals import QUIT,KEYUP,K_ESCAPE

def run_tetris_game():
    pygame.init() # Stariting the game engine
    windows_size = (640,480) 
    screen = pygame.display.set_mode(windows_size) # Create window
    pygame.display.set_caption('My Tetris') # Give title to the window
    while True:
        screen.fill((0,0,0))
        pygame.display.update()
        for event in pygame.event.get(QUIT): # quit event
            pygame.quit()
            sys.exit() # terminate if any QUIT events are present

run_tetris_game()