import pygame, time
import sys

from pygame.locals import QUIT
BLUE = (0,0,155)
BOX_SIZE = 20
SCREEN_WIDTH = 640
SCREEN_HEIGHT = 480
BOARD_WIDTH = 10

def run_tetris_game():
    pygame.init() # Stariting the game engine 
    screen = pygame.display.set_mode((SCREEN_HEIGHT,SCREEN_HEIGHT)) # Create window
    pygame.display.set_caption('My Tetris') # Give title to the window
    game_matrix = create_game_matrix() ## Create game matrix
    
    ################################
    last_time_piece_moved = time.time()
    piece = create_piece()
    ################################

    
    while True:
        screen.fill((0,0,0))


        # Move the piece one cell down after 1 second
        if(time.time()-last_time_piece_moved > 1):
            piece['row'] = piece['row'] +1
            last_time_piece_moved = time.time()

        draw_piece(screen, piece)

        pygame.draw.rect(   
            screen,
            BLUE,
            [100,50,10*20,20*20],5)  # upper left(100,50) 10*20 Width, 20*20 height
        

        pygame.display.update()

        for event in pygame.event.get(QUIT): # quit event
            pygame.quit()
            sys.exit() # terminate if any QUIT events are present

def create_piece():
    piece = {}
    piece['row'] = 0
    piece['column']  = 4
    return piece

def draw_piece(screen, piece):
    white_color = (255,255,255)
    grey_color = (217,222,226)
        
    # Bard margin + line thicknesss  (columns * box_size)
    origin_x = 100 + 5 + (piece['column']*20+1)
    origin_y = 50 + 5 + (piece['row']*20+1)
    pygame.draw.rect(screen, grey_color, [origin_x, origin_y, 20,20])
    pygame.draw.rect(screen, white_color,[origin_x, origin_y,18,18])

def create_game_matrix():
    # Columns and rows
    game_matrix_columns = 10 
    game_matrix_rows = 20
    matrix = []
    for row in range(game_matrix_rows):
        new_row = []
        for column in range(game_matrix_columns):
            new_row.append('.')
        matrix.append(new_row)
    return matrix

run_tetris_game()